// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEditor;
// using UnityEngine;
//
// public class MoreGameCreator : EditorWindow
// {
//     public MoreGameJson moreGame;
//     
//     [Serializable]
//     public class MoreGameJson
//     {
//         public string packageName;
//         public List<MoreGameItem> items;
//     }
//
//     [Serializable]
//     public class MoreGameItem
//     {
//         public string gameName;
//         public string gamePackage;
//         public string iconLink;
//         public string iconLocalPath;
//     }
// }