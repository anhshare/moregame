using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Configs/MoreGameConfig", fileName = "MoreGameConfig")]
public class MoreGameConfig : ScriptableObject
{
    public string packageName;
    public List<MoreGameItem> items;

#if UNITY_EDITOR
    [Button]
    public void UpdateInfo()
    {
        foreach (MoreGameItem gameItem in items)
        {
            gameItem.iconLink = $"https://gitlab.com/anhshare/moregame/-/raw/main/GameIcon/{gameItem.icon}.png?ref_type=heads&inline=false";
        }
    }

    public void AddAllGame()
    {
    }

    [Button]
    public void ExportToJson()
    {
        var jsonData = JsonUtility.ToJson(this);
        var path = AssetDatabase.GetAssetPath(this);
        var jsonPath = path.Replace(".asset", ".json");
        File.WriteAllText(jsonPath, jsonData);
        AssetDatabase.Refresh();
    }
#endif
}

[Serializable]
public class MoreGameItem
{
    public Sprite icon;
    public string gameName;
    public string gamePackage;
    public string iconLink;
    public string iconLocalPath;
    public int weight = 100;
}