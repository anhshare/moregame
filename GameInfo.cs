using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using System.IO;
using System.Security.Cryptography;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Configs/GameInfo", fileName = "GameInfo")]
public class GameInfo : ScriptableObject
{
    public List<GameInfoItem> games;

    [Button]
    public void UpdateInfo()
    {
        var sprites = GetAllSpriteAssetsAtPath("Assets/MoreGame/GameIcon");
        var addSpriteList = new List<Sprite>();
        foreach (var sprite in sprites)
        {
            if (games.Find(s => s.icon == sprite) == null)
            {
                addSpriteList.Add(sprite);
            }
        }

        foreach (Sprite sprite in addSpriteList)
        {
            games.Add(new GameInfoItem
            {
                icon = sprite
            });
        }

        foreach (GameInfoItem game in games)
        {
            game.iconLink = $"https://gitlab.com/anhshare/moregame/-/raw/main/GameIcon/{game.icon.name}.png";
            game.gameName = game.icon.name;
            game.md5 = CreateMD5(game.icon.texture);
        }
    }

    private static string CreateMD5(Texture2D texture2D)
    {
        return BitConverter.ToString(MD5.Create().ComputeHash(texture2D.GetRawTextureData())).Replace("-", string.Empty).ToLower();
    }

    private static List<Sprite> GetAllSpriteAssetsAtPath(string path)
    {
#if UNITY_EDITOR
        string[] paths = { path };
        var assets = AssetDatabase.FindAssets("t:sprite", paths);
        var assetsObj = assets.Select(s =>
            AssetDatabase.LoadAssetAtPath<Sprite>(AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
#endif
        return null;
    }

#if UNITY_EDITOR
    [Button]
    public void ExportToJson()
    {
        var jsonData = JsonUtility.ToJson(this);
        var path = AssetDatabase.GetAssetPath(this);
        var jsonPath = path.Replace(".asset", ".json");
        File.WriteAllText(jsonPath, jsonData);
        AssetDatabase.Refresh();
    }
#endif
}

[Serializable]
public class GameInfoItem
{
    public Sprite icon;
    public string gameName;
    public string md5;
    public string package;
    public string iconLink;

    [Button]
    public void OpenLink()
    {
        Application.OpenURL(iconLink);
    }
}